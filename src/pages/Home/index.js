import { useEffect, useState } from "react";
import MenuComponent from "../../components/menuComponent";
import PostComponent from "../../components/postComponent";
import { Box, Breadcrumbs, Container, Fab, Grid, Link, Typography } from "@mui/material";
import LoadingComponent from "../../components/loadingComponent";
import { useDispatch, useSelector } from "react-redux";
import { getAllPostRequest } from "../../redux/actions/postActions";
import AddIcon from '@mui/icons-material/Add';
import { useNavigate } from "react-router-dom";

const HomePage = () => {
    const fabStyle = {
        position: 'absolute',
        bottom: 16,
        right: 16,
      };
    const dispatch = useDispatch();
    const navigate = useNavigate()
    const posts = useSelector(state => state.post.colecao);
    const isLoading = useSelector(state => state.post.loading);

    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (e) => {
      setAnchorEl(e.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    useEffect(() => {
       dispatch(getAllPostRequest());
    }, [dispatch]);

    return (
        <>
        
        <Container maxWidth="xl">
            <MenuComponent anchorEl={anchorEl} handleClick={handleClick} handleClose={handleClose}  />
            <Breadcrumbs aria-label="breadcrumb" sx={{padding: '10px'}}>
                <Link color="inherit" href="#" onClick={(event) => event.preventDefault()}>
                Início
                </Link>
                <Typography color="text.primary">Home</Typography>
            </Breadcrumbs>
            <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            {isLoading && <LoadingComponent />}
            {posts.length > 0 && posts.map((post) => (
                <Grid item xs={2} sm={4} md={4} key={post.id}>
                    <Box sx={{ maxWidth: 400, margin: "4px" }}>
                        <PostComponent id={post.id} titulo={post.title} views={post.views} />
                    </Box>
                </Grid>
            ))}
            </Grid>
            <Fab sx={fabStyle} aria-label="Add" color="primary" onClick={()=>(navigate('/post/cadastrar'))}>
            <AddIcon />
          </Fab>
        </Container>
        <footer style={{ backgroundColor: '#DDDDDD', position: 'absolute', bottom: 0, width: '98%', height: '50px', textAlign: 'center', padding: '20px 0px 0px 50px', marginRight:'0px', marginLeft: '0px' }}>
            <Typography variant="body2" align="left">
            &copy; {new Date().getFullYear()} Luiz Leão
            </Typography>
        </footer>
        </>
    );
}

export default HomePage;
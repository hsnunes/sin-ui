import { Button, Container, Grid, TextField, Typography } from "@mui/material";
import { useState } from "react";
import MensagemComponent from "../../components/mensagemComponent";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
    const [open, setOpen] = useState(false);
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');
    const navigate = useNavigate();

    function handleLogin(e) {
        e.preventDefault();
        setOpen(true);
        navigate('/home');
    }

    function handleClose(){
        setOpen(false);
    }

    return (
        <>
            <Container maxWidth="md" style={{ position: 'relative', minHeight: '50vh' }}>
                <MensagemComponent open={open} handleClose={handleClose} titulo={email} mensagem={senha} />
                <form onSubmit={handleLogin}>
                    <Typography variant="h4" align="center" gutterBottom>
                        SIN
                    </Typography>
                    <TextField
                        label="E-mail"
                        fullWidth
                        margin="normal"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <TextField
                        label="Senha"
                        type="password"
                        fullWidth
                        margin="normal"
                        value={senha}
                        onChange={(e) => setSenha(e.target.value)}
                    />

                    <Grid container justifyContent="center">
                        <Grid item xs={12}>
                            <Button variant="contained" color="success" type="submit" fullWidth>
                            Entrar
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Container>
            <footer style={{ backgroundColor: '#DDDDDD', position: 'absolute', bottom: 0, width: '99%', height: '50px', textAlign: 'center', paddingTop: '20px', marginRight:'0px', marginLeft: '0px' }}>
                <Typography variant="body2">
                &copy; {new Date().getFullYear()} Luiz Leão
                </Typography>
            </footer>
            </>
      );
}

export default LoginPage;
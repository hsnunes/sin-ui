import { useEffect, useState } from "react";
import MenuComponent from "../../components/menuComponent";
import PostComponent from "../../components/postComponent";
import { Box, Breadcrumbs, Container, Grid, Typography } from "@mui/material";
import LoadingComponent from "../../components/loadingComponent";
import { useDispatch, useSelector } from "react-redux";
import { getPostRequest } from "../../redux/actions/postActions";
import { Link, useParams } from "react-router-dom";

const PostDetailPage = () => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const post = useSelector(state => state.post.objeto);
    const isLoading = useSelector(state => state.post.loading);

    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (e) => {
      setAnchorEl(e.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    useEffect(() => {
       dispatch(getPostRequest(id));
    }, [id, dispatch]);

    return (
        <>
        
        <Container maxWidth="xl">
            <MenuComponent anchorEl={anchorEl} handleClick={handleClick} handleClose={handleClose}  />
            <Breadcrumbs aria-label="breadcrumb" sx={{padding: '10px'}}>
                <Link color="inherit" to="/">
                Início
                </Link>
                <Typography color="text.primary">Post</Typography>
                <Typography color="text.primary">Detalhes</Typography>
            </Breadcrumbs>
            <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            {isLoading ? <LoadingComponent /> : 
            <Grid item xs={2} sm={4} md={4}>
                <Box sx={{ maxWidth: 400, margin: "4px" }}>
                    <PostComponent id={post?.id} titulo={post?.title} views={post?.views} />
                </Box>
            </Grid>
            }
            </Grid>
        </Container>
        <footer style={{ backgroundColor: '#DDDDDD', position: 'absolute', bottom: 0, width: '98%', height: '50px', textAlign: 'center', padding: '20px 0px 0px 50px', marginRight:'0px', marginLeft: '0px' }}>
            <Typography variant="body2" align="left">
            &copy; {new Date().getFullYear()} Luiz Leão
            </Typography>
        </footer>
        </>
    );
}

export default PostDetailPage;
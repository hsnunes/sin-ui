import { Badge } from "@mui/material";
import VisibilityRoundedIcon from '@mui/icons-material/VisibilityRounded';

const ViewsComponent = ({views}) => {
    return (
        <Badge badgeContent={views} color="primary">
            <VisibilityRoundedIcon color="action" />
        </Badge>
    );
}

export default ViewsComponent;
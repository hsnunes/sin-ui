import { useEffect, useState } from "react";
import MenuComponent from "../../components/menuComponent";
import { Box, Breadcrumbs, Button, Container, Grid, TextField, Typography } from "@mui/material";
import LoadingComponent from "../../components/loadingComponent";
import { useDispatch, useSelector } from "react-redux";
import { createPostRequest, getPostRequest } from "../../redux/actions/postActions";
import { Link } from "react-router-dom";

const PostForm = () => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.post.objeto);
    const isLoading = useSelector(state => state.post.loading);
    const [titulo, setTitulo] = useState('');
    const [views, setViews] = useState('');
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (e) => {
      setAnchorEl(e.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    function handleCadastrarPost(e){
        e.preventDefault();
        dispatch(createPostRequest({title: titulo, views: views}));
        alert('Post cadastrado com sucesso');
    }


    // useEffect(() => {
    //    dispatch(getPostRequest(id));
    // }, [id, dispatch]);

    return (
        <>
        <Container maxWidth="xl">
            <MenuComponent anchorEl={anchorEl} handleClick={handleClick} handleClose={handleClose}  />
            <Breadcrumbs aria-label="breadcrumb" sx={{padding: '10px'}}>
                <Link color="inherit" to="/">
                Início
                </Link>
                <Typography color="text.primary">Post</Typography>
                <Typography color="text.primary">Cadastrar</Typography>
            </Breadcrumbs>
            <form onSubmit={handleCadastrarPost}>
                <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
                    <Grid item xs={2} sm={4} md={4}>
                        <Box sx={{ maxWidth: 400, margin: "4px" }}>
                        <TextField
                                label="Título"
                                fullWidth
                                margin="normal"
                                value={titulo}
                                onChange={(e) => setTitulo(e.target.value)}
                            />
                            <TextField
                                label="Visualizações"
                                fullWidth
                                margin="normal"
                                value={views}
                                onChange={(e) => setViews(e.target.value)}
                            />
                        </Box>
                    </Grid>
                    <Grid item xs={12}>
                        <Button variant="contained" color="success" type="submit" >
                        Cadastrar
                        </Button>
                    </Grid>
                </Grid>
            </form>
            </Container>
        <footer style={{ backgroundColor: '#DDDDDD', position: 'absolute', bottom: 0, width: '98%', height: '50px', textAlign: 'center', padding: '20px 0px 0px 50px', marginRight:'0px', marginLeft: '0px' }}>
            <Typography variant="body2" align="left">
            &copy; {new Date().getFullYear()} Luiz Leão
            </Typography>
        </footer>
        </>
    );
}

export default PostForm;
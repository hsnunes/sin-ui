import Http from './http';

export async function createPost(data){
    try{
        const response = await Http.post(`/posts`, data);
        console.log('Create Post Service:', response.data);
        return true;
    } catch (error){
        throw error;
    }  
}

export async function getPost(id){
    try{
        const response = await Http.get(`/posts/${id}`);
        console.log('Post Service:', response.data);
        return response.data;
    } catch (error){
        throw error;
    }  
}

export async function getAllPost(){
    try{
        const response = await Http.get('/posts');
        console.log('Posts Service:', response.data);
        return response.data;
    } catch (error){
        throw error;
    }  
}
import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware  from 'redux-saga';
import rootReducer from './reducers/rootReducer';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: rootReducer, // Passe o rootReducer
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
      }).concat(sagaMiddleware), // Adicione o middleware do Redux Saga
});

sagaMiddleware.run(rootSaga);

export default store;
import * as types from '../actions/types';

const initialState = {
  loading: false,
  objeto: null,
  colecao: [],
  error: null,
};

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_POST_REQUEST:
      return { ...state,
              loading: true,
              error: null,
            };
    case types.GET_POST_SUCCESS:
      return { ...state,
              loading: false,
              objeto: action.payload,
              error: null
            };
    case types.GET_POST_FAILURE:
      return { ...state,
              loading: false,
              error: action.payload
            };
    
    case types.GET_ALL_POST_REQUEST:
      return { ...state,
              loading: true,
              error: null,
            };

    case types.GET_ALL_POST_SUCCESS:
      return { ...state,
              loading: false,
              colecao: action.payload,
              error: null
            };

    case types.GET_ALL_POST_FAILURE:
      return { ...state,
              loading: false,
              error: action.payload,
            };

    default:
      return state;
  }
};

export default postReducer;

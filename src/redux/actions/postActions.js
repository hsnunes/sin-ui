import * as types from './types';

export const createPostRequest = (objeto) => ({
    type: types.CREATE_POST_REQUEST,
    payload: objeto
});

export const createPostSuccess = () => ({
    type: types.CREATE_POST_SUCCESS,
    payload: true,
});

export const createPostFailure = (error) => ({
    type: types.CREATE_POST_FAILURE,
    payload: error,
});
export const getPostRequest = (id) => ({
    type: types.GET_POST_REQUEST,
    payload: {id: id}
});

export const getPostSuccess = (objeto) => ({
    type: types.GET_POST_SUCCESS,
    payload: objeto,
});

export const getPostFailure = (error) => ({
    type: types.GET_POST_FAILURE,
    payload: error,
});

export const getAllPostRequest = () => ({
    type: types.GET_ALL_POST_REQUEST
});

export const getAllPostSuccess = (colecao) => ({
    type: types.GET_ALL_POST_SUCCESS,
    payload: colecao,
});

export const getAllPostFailure = (error) => ({
    type: types.GET_ALL_POST_FAILURE,
    payload: error,
});


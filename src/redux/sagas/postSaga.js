import { call, put, takeLatest } from 'redux-saga/effects';
import * as types from '../actions/types';
import { getPost, getAllPost, createPost } from '../../services/postService';
import * as postActions from '../actions/postActions';


function* createPostSaga(action) {
    try {
        const data = action.payload;
        console.log(`Inicio CreatePost ...`);
        const post = yield call(createPost, data);
        console.log('Post:, ', post);
        yield put(postActions.createPostSuccess());
    } catch (error) {
        yield put(postActions.createPostFailure(error));
    }
}

function* getPostSaga(action) {

    try {
        const { id } = action.payload;
        console.log(`Inicio GetPost ${id} ...`);
        const post = yield call(getPost, id);
        console.log('Post:, ', post);
        yield put(postActions.getPostSuccess(post));
    } catch (error) {
      yield put(postActions.getPostFailure(error));
    }
  }

function* getAllPostSaga() {

  try {
    console.log('Inicio GetAllPost...');
    const posts = yield call(getAllPost);
    console.log('Posts Saga:, ', posts);
    yield put(postActions.getAllPostSuccess(posts));
  } catch (error) {
    yield put(postActions.getAllPostFailure(error));
  }
}

function* postSaga() {
  yield takeLatest(types.CREATE_POST_REQUEST, createPostSaga);
  yield takeLatest(types.GET_POST_REQUEST, getPostSaga);
  yield takeLatest(types.GET_ALL_POST_REQUEST, getAllPostSaga);
}

export default postSaga;

import { all } from 'redux-saga/effects';
import postSaga from './postSaga';

// Função generator que combina todas as sagas
export default function* rootSaga() {
  yield all([
    postSaga()
  ]);
}

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './pages/Home';
import LoginPage from './pages/Login';
import NotFoundPage from './pages/PageNotFound';
import PostDetailsPage from './pages/Post/postDetails';
import PostFormPage from './pages/Post/postForm';
import { Provider } from 'react-redux';
import store from './redux/store';

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<HomePage />} />
          <Route path='/login' element={<LoginPage />} />
          <Route path='/post-details/:id' element={<PostDetailsPage />} />
          <Route path='/post/cadastrar' element={<PostFormPage />} />
          <Route path='*' element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;

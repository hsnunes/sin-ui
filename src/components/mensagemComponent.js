import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";

const MensagemComponent = ({titulo, mensagem, open, handleClose}) => {
    return (

        <Dialog open={open}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">{titulo}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{mensagem}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Fechar</Button>
        </DialogActions>
      </Dialog>
    );
}

export default MensagemComponent;
import ViewsComponent from '../pages/Post/components/viewsComponent';
import { Card, CardActions, CardContent, Typography, Link } from '@mui/material';

const PostComponent = ({id, titulo, views}) => {
    return (
        <Card variant="elevation">
            <CardContent>
                <Typography variant="h5" component="div">
                {titulo} <ViewsComponent views={views} />
                </Typography>
            </CardContent>
            <CardActions>
                <Typography variant="a" component="div">
                    <Link href={`/post-details/${id}`} variant="body2">Detalhes Post</Link>
                </Typography>
            </CardActions>
        </Card>
    );
}

export default PostComponent;